## cannon-user 12 SP1A.210812.016 22.3.29 release-keys
- Manufacturer: xiaomi
- Platform: mt6853
- Codename: cannon
- Brand: Redmi
- Flavor: cannon-user
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: 22.3.29
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: zh-CN
- Screen Density: 440
- Fingerprint: Redmi/cannon/cannon:12/SP1A.210812.016/22.3.29:user/release-keys
- OTA version: 
- Branch: cannon-user-12-SP1A.210812.016-22.3.29-release-keys
- Repo: redmi_cannon_dump_18694


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
